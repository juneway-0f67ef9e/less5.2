terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
  backend "gcs" {
    bucket      = "model-choir-289513.appspot.com"
    prefix      = "terraform/state"
    credentials = "key.json"
  }
}

provider "google" {
  version     = "3.48.0"
  credentials = file(var.credentials_file_path)
  project     = var.project
  region      = var.region
  zone        = var.zone
}
