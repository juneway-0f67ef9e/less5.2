resource "google_compute_instance" "juneway" {
  count        = var.node_count
  name         = "node-${count.index + 1}"
  machine_type = "e2-small"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = 20
    }
  }
  tags = ["http-server"]
  network_interface {
    network = "default"
    access_config {
    }
  }
  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface[0].access_config[0].nat_ip} >> host.list"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "sed -i '/^${self.name}\\s/d' host.list"
  }
  provisioner "remote-exec" {
    connection {
      type        = var.remote_exec_connection_type
      user        = var.remote_exec_connection_username
      host        = self.network_interface[0].access_config[0].nat_ip
      private_key = file(var.remote_exec_connection_private_key_path)
    }
    inline = [
      "sudo apt update",
      "sudo apt install nginx -y",
      "sudo sh -c 'echo Juneway ${self.name} ${self.network_interface[0].network_ip} > /var/www/html/index.nginx-debian.html'"
    ]
  }
}
