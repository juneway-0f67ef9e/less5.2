resource "google_compute_firewall" "firewall-rule1" {
  name    = "allow-tcp-80-443"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "433"]
  }
}

resource "google_compute_firewall" "firewall-rule2" {
  name    = "allow-udp-10000-r-20000"
  network = "default"

  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  source_ranges = ["10.0.0.23"]
}
