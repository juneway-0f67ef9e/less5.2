output "project_id" {
  value = var.project
}

output "instance_names" {
  value = google_compute_instance.juneway[*].name
}
