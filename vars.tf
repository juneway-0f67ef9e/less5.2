variable "project" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "credentials_file_path" {
  type = string
}

variable "remote_exec_connection_type" {
  default = "ssh"
}

variable "remote_exec_connection_username" {
  default = "root"
}

variable "remote_exec_connection_private_key_path" {
  default = "~/.ssh/id_rsa"
}

variable "node_count" {
  default = 3
}
